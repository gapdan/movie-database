//
// Copyright (c) 2017 by Placeholder Corporation. All Rights Reserved.
//


#ifndef FILM_H_
#define FILM_H_

#include <string>
#include <vector>
#include <cmath>
#include <unordered_map>
#include <ctime>
#include <utility>

extern std::unordered_map<std::string,
                          std::vector<std::pair<double, int> > > bestYears;

class Film {
 private:
    std::string nume, id, director;
    int timestamp;
    std::vector<std::string> categories, actors_id;
    int rating;
    int nrderating;
    std::unordered_map<std::string, int> ratings;

 public:
    Film();

    Film(const std::string &nume, const std::string &id, int timestamp,
        const std::vector<std::string> &categories, const std::string &director,
        const std::vector<std::string> &actors_id);

    const std::string &getNume() const;

    void setNume(const std::string &nume);

    const std::string &getId() const;

    void setId(const std::string &id);

    const std::string &getDirector() const;

    void setDirector(const std::string &director);

    int getTimestamp() const;

    void setTimestamp(int timestamp);

    const std::vector<std::string> &getCategories() const;

    void setCategories(const std::vector<std::string> &categories);

    const std::vector<std::string> &getActors_id() const;

    void setActors_id(const std::vector<std::string> &actors_id);

    double getMedie();

    int getRating() const;

    int getNrRating();

    void setRating(std::string, int);

    void updateRating(std::string, int);

    void removeRating(std::string);
};

#endif  // FILM_H_
