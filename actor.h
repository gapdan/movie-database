//
// Copyright (c) 2017 by Placeholder Corporation. All Rights Reserved.
//


#ifndef ACTOR_H_
#define ACTOR_H_

#include <string>
#include <set>

class Actor {
 private:
    std::string actor_id, name;
    int activity;
    int first_appearence;
    int last_appearence;
    int last_rating;

 public:
    Actor();
    void setName(std::string);
    int getLastRating();
    void setLastRating(int);
    Actor(std::string, std::string);
    Actor(const Actor&);
    void setAppearence(int);
    void setLastAppearence(int);
    int getLastAppearence();
    Actor &operator=(const Actor&);
    Actor(const Actor&&);
    Actor& operator=(const Actor&&);
    void setActivity(int);
    std::string getID();
    int getActivity();
    std::string getName();
};

#endif  // ACTOR_H_
