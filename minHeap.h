//
// Copyright (c) 2017 by Placeholder Corporation. All Rights Reserved.
//


#ifndef MINHEAP_H_
#define MINHEAP_H_
#include <algorithm>
#include <utility>
#include <vector>

template<class T>
class minHeap {
 private:
    int size;
    int maxsize;
    int (*compare)(T, T);
    std::vector<T> H;

 public:
    minHeap(int (*compare)(T, T), int maxsize) : maxsize(maxsize) {
        size = 0;
        H.resize(maxsize+2);
        this->compare = compare;
    }

    int getSize() const {
        return size;
    }

    T peek(){
        if (H.size() != 0) {
            return H[1];
        } else {
            return T();
        }
    }

    int urcaHeap(int poz) {
        while (poz  > 1 && compare(H[poz], H[poz/2]) == 1) {
            std::swap(H[poz], H[poz/2]);
            poz = poz / 2;
        }
        return poz;
    }

    int addHeap(T value) {
        ++size;
        H[size] = value;
        int p = urcaHeap(size);
        return p;
    }

    int coboaraHeap(int poz) {
        int l = size;
        while ((poz * 2 <= l && compare(H[poz], H[poz * 2]) != 1) ||
        (poz * 2 + 1 <= l  && compare(H[poz], H[poz * 2 + 1]) != 1)) {
            if ((compare(H[poz*2], H[poz*2+1])) == 1 || poz * 2 + 1 > l) {
                std::swap(H[poz], H[poz*2]);
                poz = poz * 2;
            } else {
                std::swap(H[poz], H[poz * 2 + 1]);
                poz = poz * 2 + 1;
            }
        }
        return poz;
    }

    T getMin(){
        if (size >= 1) {
            T Min = H[1];
            H[1] = H[size];
            --size;
            int p = coboaraHeap(1);
            return Min;
        }
    }

    void setSize(int size) {
        minHeap::size = size;
    }
};
#endif  //  MINHEAP_H_
