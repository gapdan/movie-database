//
// Copyright (c) 2017 by Placeholder Corporation. All Rights Reserved.
//


#include <ctime>
#include <utility>
#include <string>
#include <vector>

#include "./Film.h"

Film::Film(const std::string &nume, const std::string &id, int timestamp,
    const std::vector<std::string> &categories, const std::string &director,
    const std::vector<std::string> &actors_id) : nume(nume),
    id(id),
    director(director),
    timestamp(timestamp),
    categories(categories),
    actors_id(actors_id),
    nrderating(0),
    rating(0) {}

    const std::string &Film::getNume() const {
        return nume;
    }

    void Film::setNume(const std::string &nume) {
        Film::nume = nume;
    }

    const std::string &Film::getId() const {
        return id;
    }

    void Film::setId(const std::string &id) {
        Film::id = id;
    }

    const std::string &Film::getDirector() const {
        return director;
    }

    void Film::setDirector(const std::string &director) {
        Film::director = director;
    }

    int Film::getTimestamp() const {
        return timestamp;
    }

    void Film::setTimestamp(int timestamp) {
        Film::timestamp = timestamp;
    }

    const std::vector<std::string> &Film::getCategories() const {
        return categories;
    }

    void Film::setCategories(const std::vector<std::string> &categories) {
        Film::categories = categories;
    }

    const std::vector<std::string> &Film::getActors_id() const {
        return actors_id;
    }

    void Film::setActors_id(const std::vector<std::string> &actors_id) {
        Film::actors_id = actors_id;
    }

    Film::Film(){
        nume = "";
        director = "";
        timestamp = 0;
        id = "";
        rating = 0;
        nrderating = 0;
    }

    int Film::getRating() const {
        return rating;
    }

    int Film::getNrRating() {
        return nrderating;
    }

    void Film::setRating(std::string user_id, int rat) {
        ratings[user_id] = rat;
        time_t aux = static_cast<time_t>(timestamp);
        tm ptm;
        gmtime_r(&aux, &ptm);
        int an = ptm.tm_year;
        double oldMedie = getMedie();
        Film::nrderating++;
        Film::rating = Film::rating + rat;
        double newMedie = getMedie();
        for (auto it : categories){
            if (bestYears.find(it) == bestYears.end()){
                bestYears[it] = std::vector<std::pair<double, int> >(200);
                bestYears[it][an] = std::make_pair(newMedie, 1);
            } else {
                if (oldMedie != -1) bestYears[it][an].first -= oldMedie;
                if (Film::nrderating == 1) bestYears[it][an].second += 1;
                bestYears[it][an].first += newMedie;
            }
        }
    }

    void Film::updateRating(std::string user_id, int rat) {
        time_t aux = static_cast<time_t>(timestamp);
        tm ptm;
        gmtime_r(&aux, &ptm);
        int an = ptm.tm_year;
        for (auto it : categories){
            bestYears[it][an].first -= getMedie();
        }
        int val = ratings[user_id];
        rating -= val;
        rating += rat;
        ratings[user_id] = rat;
        for (auto it : categories){
            bestYears[it][an].first += getMedie();
        }
    }

    void Film::removeRating(std::string user_id) {
        time_t aux = static_cast<time_t>(timestamp);
        tm ptm;
        gmtime_r(&aux, &ptm);
        int an = ptm.tm_year;
        for (auto it : categories){
            bestYears[it][an].first -= getMedie();
            if (nrderating == 1) bestYears[it][an].second -= 1;
        }
        int val = ratings[user_id];
        rating -= val;
        --nrderating;
        ratings.erase(user_id);
        if (nrderating > 0) {
            for (auto it : categories) {
                bestYears[it][an].first += getMedie();
            }
        }
    }

    double Film::getMedie() {
        if (nrderating != 0) {
            double d = (1.0 * rating) / (1.0 * nrderating);
            return d;
        } else {
            return -1;
        }
    }
