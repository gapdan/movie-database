//
// Copyright (c) 2017 by Placeholder Corporation. All Rights Reserved.
//

#include <string>

#include "./actor.h"

Actor::Actor() {
    actor_id = "";
    name = "";
    activity = -1;
    last_appearence = -1;
    last_rating = 0;
}

void Actor::setName(std::string nume) {
    name = nume;
}

int Actor::getLastRating(){
    return last_rating;
}

void Actor::setLastRating(int x) {
    last_rating = x;
}

Actor::Actor(std::string id, std::string nume) {
    name = nume;
    actor_id = id;
    activity = -1;
    last_appearence = -1;
    last_rating = 0;
}

Actor::Actor(const Actor &obj) {
    name = obj.name;
    actor_id = obj.actor_id;
    activity = obj.activity;
    first_appearence = obj.first_appearence;
    last_appearence = obj.last_appearence;
    last_rating = obj.last_rating;
}

void Actor::setAppearence(int val) {
    first_appearence = val;
}

void Actor::setLastAppearence(int val) {
    last_appearence = val;
}

int Actor::getLastAppearence() {
    return last_appearence;
}

Actor& Actor::operator=(const Actor &obj) {
    name = obj.name;
    actor_id = obj.actor_id;
    activity = obj.activity;;
    first_appearence = obj.first_appearence;
    last_appearence = obj.last_appearence;
    last_rating = obj.last_rating;
    return *this;
}

Actor::Actor(const Actor&& obj){
    name = obj.name;
    actor_id = obj.actor_id;
    activity = obj.activity;
    first_appearence = obj.first_appearence;
    last_appearence = obj.last_appearence;
    last_rating = obj.last_rating;
}

Actor& Actor::operator=(const Actor&& obj) {
    name = obj.name;
    actor_id = obj.actor_id;
    activity = obj.activity;
    first_appearence = obj.first_appearence;
    last_appearence = obj.last_appearence;
    last_rating = obj.last_rating;
    return *this;
}

void Actor::setActivity(int act) {
    activity = act;
}

std::string Actor::getID() {
    return actor_id;
}

int Actor::getActivity() {
    return activity;
}

std::string Actor::getName() {
    return name;
}
