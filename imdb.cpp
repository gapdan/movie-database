//
// Copyright (c) 2017 by Placeholder Corporation. All Rights Reserved.
//


#include <iterator>
#include <string>
#include <vector>
#include <unordered_map>
#include <set>
#include <queue>
#include <stack>
#include <algorithm>
#include <iomanip>
#include <sstream>
#include <map>
#include <utility>

#include "./actor.h"
#include "./Film.h"
#include "./imdb.h"
#include "./minHeap.h"

std::vector<Actor>acc;
std::unordered_map<std::string, int> HashActor, HashUser;
std::unordered_map<std::string, int> HashFilm;
std::unordered_map<std::string, int> HashDirector;
std::vector<std::set<std::string> > directorColaborations;
std::unordered_map<std::string,
                   std::vector<std::pair<double, int> > > bestYears;
std::vector<Actor>usr;
std::vector<Film>filme;
std::map<int, int> ratingYear;
std::vector<std::unordered_map<int, int> >Graph;

IMDb::IMDb() {
    MaxLongestCareer = -1;
    nrregizor = -1;
    nractor = -1;
    nruser = - 1;
    nrfilme = -1;
    influentialDirector = 0;
    std::string influentialDirectorName = "";
    std::string LongestCareerName = "";
    std::string director_name = "";
}

IMDb::~IMDb() {
}

void IMDb::add_movie(std::string movie_name,
                     std::string movie_id,
                     int timestamp,
                     std::vector<std::string> categories,
                     std::string director_name,
                     std::vector<std::string> actor_ids) {
    Film F(movie_name,
           movie_id,
           timestamp,
           categories,
           director_name,
           actor_ids);
    filme.push_back(F);
    ++nrfilme;
    ratingYear[timestamp] = nrfilme;
    HashFilm[movie_id] = nrfilme;
    if (HashDirector.find(director_name) == HashDirector.end()) {
        ++nrregizor;
        HashDirector[director_name] = nrregizor;
    }
    int dirIndex = HashDirector[director_name];
    if (dirIndex >= directorColaborations.size()) {
        directorColaborations.resize(dirIndex+5);
    }
    for (int i = 0; i < actor_ids.size(); ++i){
        directorColaborations[dirIndex].insert(actor_ids[i]);
        if (HashActor.find(actor_ids[i]) != HashActor.end()) {
            int ind = HashActor[actor_ids[i]];
            int actualTime = acc[ind].getActivity();
            if (actualTime > timestamp) {
                acc[ind].setActivity(timestamp);
            }
            actualTime = acc[ind].getLastAppearence();
            if (actualTime < timestamp) {
                acc[ind].setLastAppearence(timestamp);
            }
            if (acc[ind].getActivity() == -1) {
                acc[ind].setActivity(timestamp);
                acc[ind].setLastAppearence(timestamp);
            }
            if (MaxLongestCareer < acc[ind].getLastAppearence()
                                   - acc[ind].getActivity()) {
                MaxLongestCareer = acc[ind].getLastAppearence()
                                   - acc[ind].getActivity();
                LongestCareerName = acc[ind].getID();
            } else if (MaxLongestCareer == acc[ind].getLastAppearence()
                                         - acc[ind].getActivity()) {
                if (LongestCareerName > acc[ind].getID()) {
                    LongestCareerName = acc[ind].getID();
                }
            }
        } else {
            Actor A(actor_ids[i], "default");
            ++nractor;
            acc.push_back(A);
            HashActor[actor_ids[i]] = nractor;
            acc[nractor].setActivity(timestamp);
            acc[nractor].setLastAppearence(timestamp);
        }
    }
     for (int i = 0; i < actor_ids.size(); ++i) {
       int x = HashActor[actor_ids[i]];
        for (int j = i + 1; j < actor_ids.size(); ++j) {
            int y = HashActor[actor_ids[j]];
            if (y >= Graph.size()) {
                 Graph.resize(y + 1);
            }
            if (x >= Graph.size()) {
                Graph.resize(x + 1);
            }
            if (Graph[x].find(y) == Graph[x].end()) {
                Graph[x][y] = 1;
                Graph[y][x] = 1;
            } else {
                ++Graph[x][y];
                ++Graph[y][x];
            }
         }
     }
    if (directorColaborations[dirIndex].size() > influentialDirector) {
        influentialDirector = directorColaborations[dirIndex].size();
        influentialDirectorName = director_name;
    } else if (directorColaborations[dirIndex].size() == influentialDirector){
        influentialDirectorName = std::min(director_name,
                                           influentialDirectorName);
    }
}

void IMDb::add_user(std::string user_id, std::string name) {
    Actor A(user_id, name);
    A.setLastRating(0);
    usr.push_back(A);
    ++nruser;
    HashUser[user_id] = nruser;
}

void IMDb::add_actor(std::string actor_id, std::string name) {
    Actor A(actor_id, name);
    if (HashActor.find(actor_id) == HashActor.end()) {
        acc.push_back(A);
        ++nractor;
        HashActor[actor_id] = nractor;
    } else {
        acc[HashActor[actor_id]].setName(name);
    }
}

void IMDb::add_rating(std::string user_id, std::string movie_id, int rating) {
    int ind = HashFilm[movie_id];
    filme[ind].setRating(user_id, rating);
}

void IMDb::update_rating(std::string user_id, std::string movie_id, int rating){
    int ind = HashFilm[movie_id];
    filme[ind].updateRating(user_id, rating);
}

void IMDb::remove_rating(std::string user_id, std::string movie_id) {
    int ind = HashFilm[movie_id];
    filme[ind].removeRating(user_id);
}

std::string IMDb::get_rating(std::string movie_id) {
    int ind = HashFilm[movie_id];
    std::string s;
    double aux = filme[ind].getMedie();
    if (aux == -1) return "none";
    std::ostringstream out;
    out << std::fixed << std::setprecision(2) << aux;
    s = out.str();
    return s;
}

std::string IMDb::get_longest_career_actor() {
    return LongestCareerName;
}

std::string IMDb::get_most_influential_director() {
    if (HashDirector.size() == 0){
        return "none";
    } else {
        return influentialDirectorName;
    }
}
int nrquery = 0;

std::string IMDb::get_best_year_for_category(std::string category) {
    ++nrquery;
    double bestMedie = -1;
    int an = 0;
    bool ok = 0;
    if (bestYears.find(category) == bestYears.end()) {
        return "none";
    } else {
        int i = 0;
        for (auto it : bestYears[category]) {
            if (it.second != 0) {
                double medie = it.first / it.second;
                if (medie > bestMedie) {
                    bestMedie = medie;
                    an = i;
                    ok = 1;
                }
            }
            i++;
        }
    }
    if (ok == 0) {
        return "none";
    }
    an = 1900 + an;
    return std::to_string(an);
}

std::string IMDb::get_2nd_degree_colleagues(std::string actor_id) {
    int actor = HashActor[actor_id];
    std::unordered_map<int, bool> viz;
    std::queue<int> q;
    std::vector<std::string> sol;
    viz[actor] = 1;
    if (Graph.size() == 0){
        return "none";
    }
    if (Graph.size() <= actor) {
        return "none";
    }
    for (auto it : Graph[actor]) {
        q.push(it.first);
        viz[it.first] = 1;
    }
    while (!q.empty()) {
        int x = q.front();
        q.pop();
        for (auto it : Graph[x]) {
            if (viz[it.first] == 0) {
                viz[it.first] = 1;
                sol.push_back(acc[it.first].getID());
            }
        }
    }
    if (sol.size() == 0) return "none";
    std::sort(sol.begin(), sol.end());
    std::string afis;
    for (int i = 0; i < sol.size(); ++i) {
        for (int j = 0; j < sol[i].size(); ++j) {
            afis.push_back(sol[i][j]);
        }
        afis.push_back(' ');
    }
    afis.erase(afis.end() - 1);
    return afis;
}

int cmpmovies(std::pair<int, int> a, std::pair<int, int> b) {
    if (a.second < b.second) {
        return 1;
    } else if (a.second > b.second) {
        return 0;
    } else {
        std::string name1 = acc[a.first].getID();
        std::string name2 = acc[a.first].getID();
        if (name1 < name2) {
            return 1;
        } else {
            return 0;
        }
    }
}

std::string IMDb::get_top_k_most_recent_movies(int k) {
    int Min;
    if (filme.size() < k) {
        Min = filme.size();
    } else {
        Min = k;
    }
    if (Min == 0) {
        return "none";
    }
    minHeap< std::pair<int, int> > H(cmpmovies, Min);
    for (int i = 0; i < Min; ++i) {
        int ind = HashFilm[filme[i].getId()];
        H.addHeap(std::make_pair(ind, filme[ind].getTimestamp()));
    }
    if (k > filme.size()) {
        std::string sol;
        std::vector<int> indici;
        for (int i = 0; i < filme.size(); ++i) {
            std::pair<int, int> aux;
            aux = H.getMin();
            indici.push_back(aux.first);
        }
        for (int i = indici.size() - 1; i >= 0; --i) {
            std::string id = filme[indici[i]].getId();
            for (int j = 0; j < id.size(); ++j)
                sol.push_back(id[j]);
            sol.push_back(' ');
        }
        sol.erase(sol.end() - 1);
        return sol;
    } else {
        for (int i = Min; i < filme.size(); ++i) {
            std::pair<int, int> p;
            p = H.peek();
            std::pair<int, int>nod;
            int ind = HashFilm[filme[i].getId()];
            nod = std::make_pair(ind, filme[ind].getTimestamp());
            if (cmpmovies(p, nod) == 1) {
                H.getMin();
                H.addHeap(nod);
            }
        }
        std::string sol;
        std::vector<int> indici;
        for (int i = 0; i < k; ++i) {
            std::pair<int, int> aux;
            aux = H.getMin();
            indici.push_back(aux.first);
        }
        for (int i = indici.size() - 1; i >= 0; --i) {
            std::string id = filme[indici[i]].getId();
            for (int j = 0; j < id.size(); ++j)
                sol.push_back(id[j]);
            sol.push_back(' ');
        }
        sol.erase(sol.end() - 1);
        return sol;
    }
}

int cmpactpairs(std::pair<std::pair<int, int>, int> a,
    std::pair<std::pair<int, int>, int> b) {
        if (a.second < b.second) {
            return 1;
        } else if (a.second > b.second) {
            return 0;
        }
        std::string s1, s2;
        s1 = acc[a.first.first].getID();
        s2 = acc[a.first.second].getID();
        std::string ac1, ac2;
        ac1 = acc[b.first.first].getID();
        ac2 = acc[b.first.second].getID();
        if (s1 < ac1) {
            return 0;
        } else if (s1 > ac1) {
            return 1;
        }
        if (s2 < ac2) { return 0;
        } else if (s2 > ac2) {
            return 1;
        }
        return 1;
    }


std::string IMDb::get_top_k_actor_pairs(int k) {
    minHeap< std::pair<std::pair<int, int>, int> > H(cmpactpairs, k);
    int lg = 0;
    std::pair<std::pair<int, int>, int> aux;
    if (Graph.size() == 0){
        return "none";
    }
    for (int i = 0; i < acc.size(); ++i) {
        std::string id1 = acc[i].getID();
        int ind1 = HashActor[id1];
        if (Graph.size() > i) {
            for (auto it : Graph[i]) {
                std::string id2 = acc[it.first].getID();
                int ind2 = HashActor[id2];
                if (id1 < id2) {
                    if (lg < k) {
                        H.addHeap(std::make_pair(std::make_pair(ind1, ind2),
                        it.second));
                        ++lg;
                    } else {
                        aux = H.peek();
                        std::pair<std::pair<int, int>, int> p;
                        p = make_pair(std::make_pair(ind1, ind2), it.second);
                        if (cmpactpairs(aux, p) == 1) {
                            H.getMin();
                            H.addHeap(std::make_pair(std::make_pair(ind1, ind2),
                            it.second));
                        }
                    }
                }
            }
        }
    }
    if (H.getSize() != 0) {
        std::stack<std::pair<std::pair<int, int>, int> > st;
        std::string sol;
        while (H.getSize() > 0) {
            aux = H.getMin();
            st.push(aux);
        }
        while (!st.empty()) {
            aux = st.top();
            st.pop();
            sol.push_back('(');
            std::string id1 = acc[aux.first.first].getID();
            std::string id2 = acc[aux.first.second].getID();
            // afisez id1
            for (int i = 0; i < id1.size(); ++i) {
                sol.push_back(id1[i]);
            }
            sol.push_back(' ');
            // afisez id2
            for (int i = 0; i < id2.size(); ++i) {
                sol.push_back(id2[i]);
            }
            sol.push_back(' ');
            // afisez nr filme  (refolosesc variabila)
            id1 = std::to_string(aux.second);
            for (int i = 0; i < id1.size(); ++i) {
                sol.push_back(id1[i]);
            }
            sol.push_back(')');
            sol.push_back(' ');
        }
        sol.erase(sol.end() - 1);
        return sol;
    } else {
        return "none";
    }
}

int cmpactpartners(std::pair<int, int> a, std::pair<int, int> b) {
    if (a.second > b.second) {
        return 0;
    } else if (a.second < b.second) {
        return 1;
    }
    std::string s1, s2;
    s1 = acc[a.first].getID();
    s2 = acc[b.first].getID();
    if (s1 < s2) {
        return 0;
    } else {
        return 1;
    }
}

std::string IMDb::get_top_k_partners_for_actor(int k, std::string actor_id) {
    minHeap< std::pair<int, int> > H(cmpactpartners, k);
    int ind = HashActor[actor_id];
    int i = 0;
    if (Graph.size() == 0){
        return "none";
    }
    if (Graph.size() > ind) {
        for (auto it : Graph[ind]) {
            if (i < k) {
                H.addHeap(std::make_pair(it.first, it.second));
                ++i;
            } else {
                std::pair<int, int> aux;
                aux = H.peek();
                std::pair<int, int> p;
                p = std::make_pair(it.first, it.second);
                if (cmpactpartners(aux, p) == 1) {
                    H.getMin();
                    H.addHeap(it);
                }
            }
        }
    }
    if (H.getSize() > 0) {
        std::string sol;
        std::stack< std::pair<int, int> > st;
        std::pair<int, int> aux;
        while (H.getSize() > 0) {
            aux = H.getMin();
            st.push(aux);
        }
        while (!st.empty()) {
            aux = st.top();
            st.pop();
            std::string id = acc[aux.first].getID();
            for (i = 0; i < id.size(); ++i) {
                sol.push_back(id[i]);
            }
            sol.push_back(' ');
        }
        sol.erase(sol.end() - 1);
        return sol;
    } else {
        return "none";
    }
}

int cmpfilme(std::pair<int, int> a, std::pair<int, int> b){
    if (a.second > b.second){
         return 0;
     } else if (a.second < b.second) {
         return 1;
     }
    std::string s1, s2;
    s1 = filme[a.first].getId();
    s2 = filme[b.first].getId();
    if (s1 < s2) {
        return 0;
    } else {
        return 1;
    }
}

std::string IMDb::get_top_k_most_popular_movies(int k) {
    int Min = 0;
    int x;
    minHeap< std::pair<int, int> > H(cmpfilme, k+1);
    if (k < nrfilme + 1) {
        Min = k;
        for (int i = 0; i < k; ++i) {
            H.addHeap(std::make_pair(i, filme[i].getNrRating()));
            x = filme[i].getNrRating();
        }
        for (int i = Min; i <= nrfilme; ++i) {
            std::pair<int, int> aux;
            aux = H.peek();
            std::pair<int, int> p;
            p = std::make_pair(i, filme[i].getNrRating());
            if (cmpfilme(aux, p) == 1) {
                H.getMin();
                H.addHeap(std::make_pair(i, filme[i].getNrRating()));
            }
        }
    } else {
        for (int i = 0; i <= nrfilme; ++i) {
            H.addHeap(std::make_pair(i, filme[i].getNrRating()));
            x = filme[i].getNrRating();
        }
        Min = nrfilme + 1;
    }
    if (Min == 0) {
        return "none";
    } else {
        std::string sol;
        std::stack< std::pair<int, int> > st;
        std::pair<int, int> aux;
        Min = std::min(nrfilme+1, k);
        for (int i = 1; i <= Min; ++i) {
            aux = H.getMin();
            st.push(aux);
        }
        while (!st.empty()) {
            aux = st.top();
            st.pop();
            std::string id = filme[aux.first].getId();
            for (int j = 0; j < id.size(); ++j) {
                sol.push_back(id[j]);
            }
            sol.push_back(' ');
        }
        sol.erase(sol.end() - 1);
        return sol;
    }
}

std::string IMDb::get_avg_rating_in_range(int start, int end) {
    double sum = 0;
    int cate = 0;
    for (auto it : ratingYear) {
        if (it.first >= start && it.first <= end) {
            double x = 0;
            x = filme[it.second].getMedie();
            if (x != -1) {
                sum += x;
                ++cate;
            }
        }
    }
    double media = 0;
    if (cate != 0) {
        media = sum / cate;
    } else {
        return "none";
    }
    std::string s;
    std::ostringstream out;
    out << std::fixed << std::setprecision(2) << media;
    s = out.str();
    return s;
}
